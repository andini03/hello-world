import React, {Component, Fragment} from 'react';
import YouTubeComp from '../../../component/YouTubeComp/YouTubeComp';

class YoutubeCompPage extends Component {
    render() {
        return(
            <Fragment>
            <p>YouTube Component</p>
                <hr/>
                <YouTubeComp 
                    time="7.12" 
                    title="Keindahan Alam-1"
                    desc="15x ditonton 2 hari yang lalu"/>
                <YouTubeComp 
                    time="5.34" 
                    title="Keindahan Alam-2"
                    desc="10x ditonton 2 hari yang lalu"/>
                <YouTubeComp 
                    time="6.14" 
                    title="Keindahan Alam-3"
                    desc="6x ditonton 2 hari yang lalu"/>
                <YouTubeComp 
                    time="4.28" 
                    title="Keindahan Alam-4"
                    desc="10x ditonton 2 hari yang lalu"/>
                <YouTubeComp 
                    time="5.31"
                    title="Keindahan Alam-5"
                    desc="11x ditonton 2 hari yang lalu"/>
                <YouTubeComp /> 
                </Fragment>
        )
    }
}

export default YoutubeCompPage;