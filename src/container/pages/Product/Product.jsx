import React, {Component, Fragment} from 'react';
import './Product.css';
import CardProduct from './CardProduct/CardProduct';

class Product extends Component {
    state= {
        order: 4
    }

    handleCounterChange = (newValue) => {
        this.setState({
            order: newValue
        })
    }

    render(){
        return(
            <Fragment>
                <p>Halaman Product</p>
                <div className="header">
                    <div className="logo">
                    <img src="https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/jznhmlcmc5dia2qrmc7r" alt="" />
                </div>
                    <div className="troley">
                        <img src="https://www.logolynx.com/images/logolynx/98/98a7024d8bb4455ce1f48880bd51cdd4.jpeg" alt="" />
                        <div className="count">{this.state.order}</div>
                    </div>
                </div>
 
                <CardProduct onCounterChange={(value)=> this.handleCounterChange(value)}/> 
           </Fragment>
        );
    }
}

export default Product;
